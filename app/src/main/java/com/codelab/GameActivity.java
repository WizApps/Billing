package com.codelab;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.StringRes;
import android.support.annotation.UiThread;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;

import com.codelab.billing.BillingManager;
import com.codelab.billing.BillingProvider;
import com.codelab.sample.R;
import com.codelab.skulist.PurchaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Example game using Play Billing library.
 * <p>
 * Please follow steps inside the codelab to understand the best practices for this new library.
 */
public class GameActivity extends FragmentActivity implements BillingProvider {

    @BindView(R.id.free_or_premium) ImageView mAccountType;
    @BindView(R.id.gas_gauge) ImageView mGasImageView;

    private BillingManager mBillingManager;
    private PurchaseFragment mPurchaseFragment;
    private GameController mGameController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);

        // Start the controller and load game data
        mGameController = new GameController(this);

        // Try to restore dialog fragment if we were showing it prior to screen rotation
        if (savedInstanceState != null) {
            mPurchaseFragment = (PurchaseFragment) getSupportFragmentManager().findFragmentByTag(PurchaseFragment.TAG);
        }

        // Create and initialize BillingManager which talks to BillingLibrary
        mBillingManager = new BillingManager(this);
        updateUi();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBillingManager.destroy();
    }

    @Override
    public BillingManager getBillingManager() {
        return mBillingManager;
    }

    @OnClick({R.id.button_drive, R.id.button_purchase})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_drive:
                onDriveButtonClicked(view);
                break;
            case R.id.button_purchase:
                onPurchaseButtonClicked(view);
                break;
        }
    }

    /**
     * Drive button clicked. Burn gas!
     */
    public void onDriveButtonClicked(View arg0) {
        if (mGameController.isTankEmpty()) {
            alert(R.string.alert_no_gas);
        } else {
            mGameController.useGas();
            alert(R.string.alert_drove);
            updateUi();
        }
    }

    /**
     * User clicked the "Buy Gas" button - show a purchase dialog with all available SKUs
     */
    public void onPurchaseButtonClicked(final View arg0) {
        if (mPurchaseFragment == null) {
            mPurchaseFragment = new PurchaseFragment();
        }

        if (!isAcquireFragmentShown()) {
            mPurchaseFragment.show(getSupportFragmentManager(), PurchaseFragment.TAG);
        }
    }

    /**
     * Show an alert dialog to the user
     *
     * @param messageId String id to display inside the alert dialog
     */
    @UiThread
    void alert(@StringRes int messageId) {
        if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
            throw new RuntimeException("Dialog could be shown only from the main thread");
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.setMessage(messageId);
        builder.create().show();
    }

    /**
     * Refresh the UI & update gas gauge to reflect tank status
     */
    @UiThread
    private void updateUi() {
        mGasImageView.setImageResource(mGameController.getTankResId());

        if (isAcquireFragmentShown()) {
            mPurchaseFragment.refreshUi();
        }
    }

    public boolean isAcquireFragmentShown() {
        return mPurchaseFragment != null && mPurchaseFragment.isVisible();
    }
}