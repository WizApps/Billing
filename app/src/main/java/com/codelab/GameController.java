package com.codelab;

import android.content.SharedPreferences;
import android.support.annotation.DrawableRes;

import com.codelab.sample.R;

import static android.content.Context.MODE_PRIVATE;

/**
 * Handles control logic of the GameActivity
 */
class GameController {

    // How many units (1/4 tank is our unit) fill in the tank.
    private static final int TANK_MAX = 4;

    // Graphics for the gas gauge
    private static int[] TANK_RES_IDS = {
            R.drawable.gas0,
            R.drawable.gas1,
            R.drawable.gas2,
            R.drawable.gas3,
            R.drawable.gas4};

    // Current amount of gas in tank, in units
    private int mTank;

    // Activity instance
    private GameActivity mActivity;

    GameController(GameActivity activity) {
        mActivity = activity;
        loadData();
    }

    void useGas() {
        mTank--;
        saveData();
    }

    boolean isTankEmpty() {
        return mTank <= 0;
    }

    boolean isTankFull() {
        return mTank >= TANK_MAX;
    }

    @DrawableRes
    int getTankResId() {
        int index = (mTank >= TANK_RES_IDS.length) ? (TANK_RES_IDS.length - 1) : mTank;
        return TANK_RES_IDS[index];
    }

    /**
     * Save current tank level to disc
     * <p>
     * Note: In a real application, we recommend you save data in a secure way to prevent tampering.
     * For simplicity in this sample, we simply store the data using a SharedPreferences.
     */
    private void saveData() {
        SharedPreferences.Editor spe = mActivity.getPreferences(MODE_PRIVATE).edit();
        spe.putInt("tank", mTank);
        spe.apply();
    }

    private void loadData() {
        SharedPreferences sp = mActivity.getPreferences(MODE_PRIVATE);
        mTank = sp.getInt("tank", 3);
    }
}