package com.codelab.billing;

public interface BillingConstants {

    String SKU_GAS = "gas";
    String SKU_PREMIUM = "premium";
    String SKU_MONTHLY = "gold_monthly";
    String SKU_YEARLY = "gold_yearly";
}