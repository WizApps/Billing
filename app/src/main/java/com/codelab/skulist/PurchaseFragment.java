package com.codelab.skulist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.codelab.billing.BillingProvider;
import com.codelab.sample.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Displays a screen with various in-app purchase and subscription options
 */
public class PurchaseFragment extends DialogFragment implements SkuDetailsResponseListener {

    public static final String TAG = PurchaseFragment.class.getSimpleName();

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.list) RecyclerView mRecyclerView;
    @BindView(R.id.error_textview) TextView mErrorTextView;
    @BindView(R.id.progress_bar) View mProgress;

    private BillingProvider mBillingProvider;
    private SkuAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_purchase, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mToolbar.setTitle(R.string.button_purchase);
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        setWaitScreen(true);
        onManagerReady((BillingProvider) getActivity());
    }

    @Override
    public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {
        if (responseCode == BillingClient.BillingResponse.OK && skuDetailsList != null) {
            List<SkuModel> skuList = new ArrayList<>();
            for (SkuDetails details : skuDetailsList) {
                SkuModel model = new SkuModel(
                        details.getSku(),
                        details.getTitle(),
                        details.getPrice(),
                        details.getDescription(),
                        details.getType());
                skuList.add(model);
            }

            if (skuList.size() == 0) {
                displayAnErrorIfNeeded();
            } else {
                mAdapter.updateData(skuList);
                setWaitScreen(false);
            }
        }
    }

    /**
     * Notifies the fragment that billing manager is ready and provides a BillingProvider
     * instance to access it
     */
    public void onManagerReady(BillingProvider billingProvider) {
        mBillingProvider = billingProvider;
        mAdapter = new SkuAdapter(mBillingProvider);
        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        }
        handleManagerAndUiReady();
    }

    /**
     * Executes query for SKU details at the background thread
     */
    private void handleManagerAndUiReady() {
        // Start querying for in-app SKUs
        List<String> skusList = mBillingProvider.getBillingManager().getSkus(BillingClient.SkuType.INAPP);
        mBillingProvider.getBillingManager().querySkuDetailsAsync(BillingClient.SkuType.INAPP, skusList, this);

        // Start querying for subscriptions SKUs
        skusList = mBillingProvider.getBillingManager().getSkus(BillingClient.SkuType.SUBS);
        mBillingProvider.getBillingManager().querySkuDetailsAsync(BillingClient.SkuType.SUBS, skusList, this);
    }

    private void displayAnErrorIfNeeded() {
        if (getActivity() == null || getActivity().isFinishing()) {
            Log.d(TAG, "No need to show an error - activity is finishing already");
            return;
        }

        mProgress.setVisibility(View.GONE);
        mErrorTextView.setVisibility(View.VISIBLE);
        mErrorTextView.setText(getText(R.string.error_codelab_not_finished));

        // TODO: Here you will need to handle various respond codes from BillingManager
    }

    /**
     * Enables or disables "please wait" screen.
     */
    private void setWaitScreen(boolean set) {
        mRecyclerView.setVisibility(set ? View.GONE : View.VISIBLE);
        mProgress.setVisibility(set ? View.VISIBLE : View.GONE);
    }

    /**
     * Refreshes this fragment's UI
     */
    public void refreshUi() {
        Log.d(TAG, "Looks like purchases list might have been updated - refreshing the UI");
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }
}