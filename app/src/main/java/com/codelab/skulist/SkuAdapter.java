package com.codelab.skulist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codelab.billing.BillingProvider;
import com.codelab.sample.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for a RecyclerView that shows SKU details for the app.
 * <p>
 * Note: It's done fragment-specific logic independent and delegates control back to the
 * specified handler (implemented inside PurchaseFragment in this example)
 * </p>
 */
public class SkuAdapter extends RecyclerView.Adapter<SkuHolder> implements SkuHolder.OnButtonClickListener {

    private List<SkuModel> mListData;
    private BillingProvider mBillingProvider;

    SkuAdapter(BillingProvider billingProvider) {
        mBillingProvider = billingProvider;
    }

    void updateData(List<SkuModel> data) {
        if (mListData == null) mListData = new ArrayList<>();
        mListData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public SkuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_sku, parent, false);
        return new SkuHolder(item, this);
    }

    @Override
    public void onBindViewHolder(SkuHolder holder, int position) {
        SkuModel model = mListData.get(position);
        if (model != null) holder.bindData(model);
    }

    @Override
    public int getItemCount() {
        return mListData == null ? 0 : mListData.size();
    }

    @Override
    public void onButtonClicked(int position) {
        SkuModel data = mListData.get(position);
        mBillingProvider.getBillingManager().startPurchaseFlow(data.getSku(), data.getBillingType());
    }
}