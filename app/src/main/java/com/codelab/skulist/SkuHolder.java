package com.codelab.skulist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.codelab.billing.BillingConstants;
import com.codelab.sample.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * ViewHolder for quick access to row's views
 */
class SkuHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.title) TextView title;
    @BindView(R.id.price) TextView price;
    @BindView(R.id.description) TextView description;
    @BindView(R.id.state_button) Button button;
    @BindView(R.id.sku_icon) ImageView skuIcon;

    /**
     * Handler for a button click on particular row
     */
    public interface OnButtonClickListener {
        void onButtonClicked(int position);
    }

    SkuHolder(final View itemView, final OnButtonClickListener clickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        if (button != null) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onButtonClicked(getAdapterPosition());
                }
            });
        }
    }

    void bindData(SkuModel model) {
        title.setText(model.getTitle());
        description.setText(model.getDescription());
        price.setText(model.getPrice());
        button.setEnabled(true);

        switch (model.getSku()) {
            case BillingConstants.SKU_GAS:
                skuIcon.setImageResource(R.drawable.gas_icon);
                break;
            case BillingConstants.SKU_PREMIUM:
                skuIcon.setImageResource(R.drawable.premium_icon);
                break;
            case BillingConstants.SKU_MONTHLY:
            case BillingConstants.SKU_YEARLY:
                skuIcon.setImageResource(R.drawable.gold_icon);
                break;
        }
    }
}