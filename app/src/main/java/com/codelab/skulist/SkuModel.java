package com.codelab.skulist;

/**
 * A model for SkuAdapter's row which holds all the data to render UI
 */
class SkuModel {

    private final String sku, title, price, description, billingType;

    SkuModel(String sku, String title, String price, String description, String type) {
        this.sku = sku;
        this.title = title;
        this.price = price;
        this.description = description;
        this.billingType = type;
    }

    String getSku() {
        return sku;
    }

    String getTitle() {
        return title;
    }

    String getPrice() {
        return price;
    }

    String getDescription() {
        return description;
    }

    String getBillingType() {
        return billingType;
    }
}